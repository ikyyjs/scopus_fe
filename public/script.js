var apiKey = "7f59af901d2d86f78a1fd60c1bf9426a";

function fetchData() {
  var query = document.getElementById("search").value;
  var url = `https://api.elsevier.com/content/search/sciencedirect?query=${encodeURIComponent(
    query
  )}&apiKey=${encodeURIComponent(apiKey)}`;

  fetch(url)
    .then((response) => response.json())
    .then((data) => {
      var resultsDiv = document.getElementById("results");
      resultsDiv.innerHTML = ""; // Clear previous results

      var totalResults = data["search-results"]["opensearch:totalResults"];
      resultsDiv.innerHTML += `<p>Total hasil: ${totalResults}</p>`;

      var entries = data["search-results"]["entry"];
      entries.forEach((entry) => {
        var title = entry["dc:title"];
        var creator = entry["dc:creator"];
        var publicationName = entry["prism:publicationName"];
        var year = entry["prism:coverDate"].split("-")[0];
        var volume = entry["prism:volume"];

        var entryDiv = document.createElement("div");
        entryDiv.classList.add(
          "mt-4",
          "shadow-lg",
          "bg-gray-200",
          "p-8",
          "rounded-lg",
          "hover:bg-green-500",
          "hover:text-white",
          "transition",
          "duration-300",
          "cursor-pointer",
          "mb-4"
        ); // Added margin-bottom class

        entryDiv.innerHTML = `
          <h2 class="text-xl font-semibold">${title}</h2>
          <p class="text-gray-600"><strong>Creator:</strong> ${creator}</p>
          <p class="text-gray-600"><strong>Publication Name:</strong> ${publicationName}</p>
          <p class="text-gray-600"><strong>Year - Volume:</strong> ${year} - ${volume}</p>
          
        `;

        resultsDiv.appendChild(entryDiv);
      });
    })
    .catch((error) => {
      console.error("Error fetching data:", error);
    });
}

function exportToCSV() {
  var entries = document.querySelectorAll("#results > div");
  var csvContent = "data:text/csv;charset=utf-8,";

  entries.forEach((entry) => {
    var title = entry.querySelector("h2").innerText;
    var creator = entry
      .querySelector("p:nth-of-type(1)")
      .innerText.split(":")[1]
      .trim();
    var publicationName = entry
      .querySelector("p:nth-of-type(2)")
      .innerText.split(":")[1]
      .trim();
    var year = entry
      .querySelector("p:nth-of-type(3)")
      .innerText.split(":")[1]
      .trim()
      .split(" - ")[0];
    var volume = entry
      .querySelector("p:nth-of-type(3)")
      .innerText.split(":")[1]
      .trim()
      .split(" - ")[1];

    var row = [title, creator, publicationName, year, volume].join(",");
    csvContent += row + "\r\n";
  });

  var encodedUri = encodeURI(csvContent);
  var link = document.createElement("a");
  link.setAttribute("href", encodedUri);
  link.setAttribute("download", "scopus_search_results.csv");
  document.body.appendChild(link);
  link.click();
}

function sendDataToBackend() {
  var entries = document.querySelectorAll("#results > div");
  var sendData = [];

  entries.forEach((entry) => {
    var title = entry.querySelector("h2").innerText;
    var creator = entry
      .querySelector("p:nth-of-type(1)")
      .innerText.split(":")[1]
      .trim();
    var publicationName = entry
      .querySelector("p:nth-of-type(2)")
      .innerText.split(":")[1]
      .trim();
    var year = entry
      .querySelector("p:nth-of-type(3)")
      .innerText.split(":")[1]
      .trim()
      .split(" - ")[0];
    var volume = entry
      .querySelector("p:nth-of-type(3)")
      .innerText.split(":")[1]
      .trim()
      .split(" - ")[1];

    sendData.push({
      title: title,
      creator: creator,
      publicationName: publicationName,
      year: year,
      volume: volume,
    });
  });

  fetch("http://localhost:3000/scopus", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(sendData),
  })
    .then((response) => {
      if (response.ok) {
        console.log("Data terkirim ke backend:", sendData);
        alert("Data berhasil terkirim ke backend!");
      } else {
        throw new Error("Gagal mengirim data ke backend.");
      }
    })
    .catch((error) => {
      console.error("Error:", error);
      alert("Gagal mengirim data ke backend. Silakan coba lagi nanti.");
    });
}
